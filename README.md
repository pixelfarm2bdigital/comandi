## Initieële setup

Zet je project op zoals normaal, dus `wp-config.local.php` aanpassen etc. Als dit is gedaan, en je Wordpress draait kan je onderstaande stappen doorlopen.


#### Install gulp and Bower

Building the theme requires [node.js](http://nodejs.org/download/). We recommend you update to the latest version of npm: `npm install -g npm@latest`.

From the command line:

1. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`
2. Navigate to the theme directory, then run `npm install`
3. Run `bower install`

You now have all the necessary dependencies to run the build process.

1. Run `gulp` for the first time, the folder `resources` should be created. 
2. Edit the line `"devUrl": "http://dev.wordpress-template.nl"` in `_develop\assets\manifest.json` to reflect your local URL.
3. Run `gulp watch` to initialize browsersync and init the watcher.

#### Available GULP commands

* `gulp` — Compile and optimize the files in your assets directory 
* `gulp watch` — Compile assets when file changes are made
* `gulp --production` — Compile assets for production (no source maps) and unique hash.

## Assets
* `_develop/assets` Hier staan alle assets die naar `resources` gecompiled worden. Deze map zal bij het aanmaken van het project er niet zijn.
* `media/uploads` Uploads gaan in deze map


## WP CLI
In `plugins` zit een plugin genaamd `advanced-custom-fields-wpcli` deze plugin zorgt er voor dat de ACF velden via [WP CLI](https://wp-cli.org/#installing) naar `media\field-groups` geexporteerd worden. Om dit te kunnen gebruiken moet je dus WP CLI installeren, uitleg hiervoor staat op [WP CLI](https://wp-cli.org/#installing).

### WP CLI Commands
* `wp` hiermee krijg je een overzicht van alle mogelijke commands
* `wp search-replace string1 string2 --all-tables` in je WP database een string vervangen, handig om dev naar productie-urls om te zetten. Eventueel `--dry-run` toevoegen om eerst te testen
* `wp acf export`, `wp acf clean`, `wp acf import`. Gebruikt om ACF velden te importeren, exporteren en op te ruimen uit je database.
* `wp plugin list` lijst met actieve plugins zien
* `wp core version` Huidige versie van WP zien, eventueel updaten met `wp core update`
* `wp scaffold` om bijvoorbeeld een post type of plugin te scaffolden
* `wp db import` om snel een database te importeren (of exporteren).
