<?php

/* Site state */

require_once 'wp-config.local.php';

define('VERSION', '0.0.1');

if (!defined('DEVELOPMENT_MODE')) define('DEVELOPMENT_MODE', false);

if(!DEVELOPMENT_MODE) {
    define('DOMAIN', $_SERVER['HTTP_HOST']);
    define('DOMAIN_URL', 'https://' . DOMAIN);
}

define('CONTENT_URL', DOMAIN_URL . '/resources');
define('RESOURCES_DIR', ABSPATH . 'resources');

/* Email addresses */
define('EMAIL_DEVELOPMENT_TEAM', 'development@pixelfarm.nl');


/* - - - - - - - - - - - - - - - - FROM HEREON DON'T EDIT ANYTHING - - - - - - - - - - - - - - - - */


/* Force SSL client-side when server-side fails */
if(!DEVELOPMENT_MODE) { // remove need for SSL on development enviroments
    if (!(
        isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1)
        ||
        (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
    )) {
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: https://' . DOMAIN . '/');
        exit();
    }
}


/* Default Php settings */
define('PIXELFARM_TIMEZONE', 'Europe/Amsterdam');
date_default_timezone_set(PIXELFARM_TIMEZONE);
ini_set('default_charset', 'UTF-8');
if (!defined('ABSPATH')) {
    define('ABSPATH', __DIR__ . '/');
}


/* Security */
ini_set('expose_php', 0);
header_remove('X-Powered-By');
ini_set('session.cookie_httponly', 1);


/* Error reporting */
ini_set('log_errors', 'Off');
define('WP_DEBUG_LOG', false);
if (DEVELOPMENT_MODE) {
    ini_set('display_errors', 'On');
    ini_set('error_reporting', E_ALL);
    define('WP_DEBUG', true);
    define('WP_DEBUG_DISPLAY', true);
    define('VERSION_URI', time());
} else {
    ini_set('display_errors', 'Off');
    ini_set('error_reporting', 0);
    define('WP_DEBUG', false);
    define('WP_DEBUG_DISPLAY', false);
    define('VERSION_URI', substr(md5(VERSION), 0, 6));
}


/* WordPress settings */
define('COOKIE_DOMAIN', DOMAIN);
define('WP_SITEURL', DOMAIN_URL);
define('WP_HOME', DOMAIN_URL);
define('LD', 'pixelfarm');

define('WP_DEFAULT_THEME', 'pixelfarm');
define('AUTOMATIC_UPDATER_DISABLED', true);
define('WP_AUTO_UPDATE_CORE', false);
define('DISALLOW_FILE_EDIT', true);
define('EMPTY_TRASH_DAYS', 9999999);
define('WP_MEMORY_LIMIT', '64M');
define('WP_POST_REVISIONS', 5);

define('WP_CONTENT_FOLDERNAME', 'media');
define('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
define('WP_CONTENT_URL', WP_SITEURL . '/' . WP_CONTENT_FOLDERNAME);
define ('WP_MEDIA_URL', WP_SITEURL . '/media');
define('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins');
define('WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins');
define('ICONS', CONTENT_URL . '/img/icons.' . VERSION_URI . '.svg');

define('AUTH_KEY', 'put your unique phrase here');
define('SECURE_AUTH_KEY', 'put your unique phrase here');
define('LOGGED_IN_KEY', 'put your unique phrase here');
define('NONCE_KEY', 'put your unique phrase here');
define('AUTH_SALT', 'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT', 'put your unique phrase here');
define('NONCE_SALT', 'put your unique phrase here');


/* Require private settings */



//!TODO 2. Do WordPress installation
//!TODO 3. Move all defines under this block to wp-config.local.php
define('DB_NAME', 'database_name_here');
define('DB_USER', 'username_here');
define('DB_PASSWORD', 'password_here');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
$table_prefix = 'pxl_';


/* Initialize WordPress */
require_once(ABSPATH . 'wp-settings.php');
