<?php
get_header();

// Get news items, although its already the archive page.
// We want the items in a specific order!
$results = get_custom_post_type_posts('newsitem', [
    'orderby' => 'date',
    'order'   => 'DESC',
]);

// We need to be able to override the default $post in our foreach loop
global $post;
?>
    <article class="news archive" itemscope itemtype="https://schema.org/CreativeWork">
        <header>
            <h1 itemprop="headline"><?php _e('News', LD) ?></h1>
        </header>
        <div class="content-wrapper section" itemprop="text">
            <section>
                <?php if (!empty($newsitems)) : ?>
                    <div class="items">
                        <?php foreach ($newsitems as $newsitem) : ?>
                            <?php
                            $post = $newsitem;
                            setup_postdata($post);

                            $title        = get_the_title();
                            $imgAttrAlt   = get_seo_title($title);
                            $imgAttrTitle = get_seo_title($title);
                            $singleLink   = get_the_permalink();
                            ?>
                            <div>
                                <?php // newsitem info ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php else: ?>
                    <div class="not-found"><?php _e('No newsitems found.', LD) ?></div>
                <?php endif; ?>
            </section>
        </div>
    </article>
<?php
wp_reset_query();
get_footer();
