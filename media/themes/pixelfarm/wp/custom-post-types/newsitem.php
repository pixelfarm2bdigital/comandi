<?php

class Pixelfarm_CPT_Newsitem
{
    private static $instance = null;
    private $cptname;
    private $cptnamePlural;
    private $rewrite;
    private $title;

    public static function get_instance()
    {
        if (null === self::$instance) {
            $class = __CLASS__;
            new $class;
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->cptname       = 'newsitem';
        $this->cptnamePlural = 'newsitems';
        $this->rewrite       = 'nieuws';
        $this->title         = __('newsitem', LD);

        add_action('init', [&$this, 'newsitem_register_post_type']);
        add_action('admin_init', [&$this, 'admin_init']);
        add_filter('enter_title_here', [&$this, 'enter_title_here']);
        add_filter('pre_get_posts', [&$this, 'pre_get_posts']);
    }

    /**
     * Register custom post type 'newsitem'.
     */
    public function newsitem_register_post_type()
    {
        $args = [
            'labels'              => [
                'name'               => __('News', LD),
                'singular_name'      => __('Newsitem', LD),
                'menu_name'          => __('News', LD),
                'add_new'            => __('New newsitem', LD),
                'add_new_item'       => __('New newsitem', LD),
                'new_item'           => __('New newsitem', LD),
                'edit_item'          => __('Edit newsitem', LD),
                'view_item'          => __('View newsitem', LD),
                'search_items'       => __('Search newsitem', LD),
                'not_found'          => __('No newsitems found', LD),
                'not_found_in_trash' => __('No newsitems found in thrash', LD),
                'all_items'          => __('All newsitems', LD),
            ],
            'description'         => '',
            'public'              => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_nav_menus'   => false,
            'show_in_menu'        => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 21,
            'menu_icon'           => 'dashicons-media-document',
            'hierarchical'        => false,
            'has_archive'         => true,
            'query_var'           => false,
            'can_export'          => true,
            'capability_type'     => $this->cptname,
            'map_meta_cap'        => true,
            'capabilities'        => [

                // Meta capabilities
                'edit_post'              => "edit_{$this->cptname}",
                'read_post'              => "read_{$this->cptname}",
                'delete_post'            => "delete_{$this->cptname}",

                // Primitive capabilities used outside of map_meta_cap():
                'edit_posts'             => "edit_{$this->cptnamePlural}",
                'edit_others_posts'      => "edit_others_{$this->cptnamePlural}",
                'publish_posts'          => "publish_{$this->cptnamePlural}",
                'read_private_posts'     => "read_private_{$this->cptnamePlural}",

                // Primitive capabilities used within map_meta_cap():
                'read'                   => "read",
                'delete_posts'           => "delete_{$this->cptnamePlural}",
                'delete_private_posts'   => "delete_private_{$this->cptnamePlural}",
                'delete_published_posts' => "delete_published_{$this->cptnamePlural}",
                'delete_others_posts'    => "delete_others_{$this->cptnamePlural}",
                'edit_private_posts'     => "edit_private_{$this->cptnamePlural}",
                'edit_published_posts'   => "edit_published_{$this->cptnamePlural}",
                'create_posts'           => "edit_{$this->cptnamePlural}",
            ],
            'supports'            => ['title', 'editor', 'thumbnail', 'revisions'],
            'rewrite'             => ['slug' => $this->rewrite, 'with_front' => false],
        ];

        register_post_type($this->cptname, $args);
    }


    /**
     * Give administrator permission to this new custom post type.
     */
    public function admin_init()
    {
        $admin = get_role('administrator');

        if (!empty($admin)) {
            $admin->add_cap("edit_{$this->cptname}");
            $admin->add_cap("read_{$this->cptname}");
            $admin->add_cap("delete_{$this->cptname}");

            $admin->add_cap("edit_{$this->cptnamePlural}");
            $admin->add_cap("edit_others_{$this->cptnamePlural}");
            $admin->add_cap("publish_{$this->cptnamePlural}");
            $admin->add_cap("read_private_{$this->cptnamePlural}");

            $admin->add_cap("read");
            $admin->add_cap("delete_{$this->cptnamePlural}");
            $admin->add_cap("delete_private_{$this->cptnamePlural}");
            $admin->add_cap("delete_published_{$this->cptnamePlural}");
            $admin->add_cap("delete_others_{$this->cptnamePlural}");
            $admin->add_cap("edit_private_{$this->cptnamePlural}");
            $admin->add_cap("edit_published_{$this->cptnamePlural}");
            $admin->add_cap("edit_{$this->cptnamePlural}");
        }
    }

    /**
     * Change placeholder of titlebar for this custom post type.
     *
     * @param string $input
     *
     * @return string
     */
    public function enter_title_here($input)
    {
        global $post_type;

        if (is_admin() && $this->cptname === strtolower($post_type)) {
            $input = sprintf(__('Enter title of %s', LD), __($this->title, LD));
        }

        return $input;
    }

    /**
     * Order posts by title in the wp-admin.
     *
     * @param \WP_Query $wp_query
     */
    public function pre_get_posts($wp_query)
    {
        if (is_admin()) {
            $post_type = $wp_query->query['post_type'] ?? false;

            if ($this->cptname === $post_type) {
                $wp_query->set('orderby', 'title');
                $wp_query->set('order', 'ASC');
            }
        }
    }
}

Pixelfarm_CPT_Newsitem::get_instance();
