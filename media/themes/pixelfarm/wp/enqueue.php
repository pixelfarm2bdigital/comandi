<?php

// Frontend
if (!is_admin()) {
    add_action('wp_enqueue_scripts', 'frontend_load_files');

    // Disable WooCommerc CSS
    add_filter('woocommerce_enqueue_styles', '__return_empty_array');
    remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
}

// Login (wp-admin)
if (in_array($GLOBALS['pagenow'], ['wp-login.php', 'wp-register.php'])) {
    add_action('login_enqueue_scripts', 'login_load_files');
}

// Backend (wp-admin)
if (is_admin()) {
    add_action('admin_enqueue_scripts', 'admin_load_files');
}

/**
 * Load conditional scripts and stylesheets for the frontend.
 */
function frontend_load_files()
{
    /* - - - - - - - - - - - - - - - - - STYLES - - - - - - - - - - - - - - - - - */


    /* - - - - - - - - - - - - - - - - - STYLES - - - - - - - - - - - - - - - - - */
//    // Owl carousel
//    wp_deregister_style('owl-carousel-css');
//    wp_register_style('owl-carousel-css', WP_MEDIA_URL . '/lib/owl-carousel/assets/owl.carousel.css', VERSION_URI,
//        false);
//    wp_enqueue_style('owl-carousel-css');

    // Animate css
//    wp_deregister_style('animate-css');
//    wp_register_style('animate-css', WP_MEDIA_URL . '/lib/animatecss/animate.css', VERSION_URI,
//        false);
//    wp_enqueue_style('animate-css');

    // Fontawesome css
//    wp_deregister_style('fontawesome-css');
//    wp_register_style('fontawesome-css', WP_MEDIA_URL . '/lib/fontawesome/css/font-awesome.min.css', VERSION_URI,
//        false);
//    wp_enqueue_style('fontawesome-css');

//    // Stylesheet
//    wp_dequeue_style('style');
//    wp_deregister_style('style');
//    wp_register_style('style', CONTENT_URL . '/styles/main.css', [], null);
//    wp_enqueue_style('style');
//

    /* - - - - - - - - - - - - - - - - - SCRIPTS - - - - - - - - - - - - - - - - - */
//
//    $jsArgs = [];
//
//    // jQuery
//    wp_dequeue_script('jquery');
//    wp_deregister_script('jquery');
//    wp_register_script('jquery', CONTENT_URL . '/scripts/jquery.js', [], VERSION_URI, false);
//    wp_enqueue_script('jquery');
//    $jsArgs[] = 'jquery';
//
//    // Main document
//    wp_deregister_script('main');
//    wp_register_script('main', CONTENT_URL . '/scripts/main.js', $jsArgs, VERSION_URI, false);
//    wp_enqueue_script('main');

//    // svg4everybody
//    wp_deregister_script('svg4everybody');
//    wp_register_script('svg4everybody', CONTENT_URL . '/js/svg4everybody.js', ['main'], VERSION_URI, false);
//    wp_enqueue_script('svg4everybody');
//    wp_add_inline_script('svg4everybody', 'svg4everybody();');

//    // Owl carousel
//    wp_deregister_script('owl-carousel-js');
//    wp_register_script('owl-carousel-js', WP_MEDIA_URL . '/lib/owl-carousel/owl.carousel.js', $jsArgs, VERSION_URI,
//        false);
//    wp_enqueue_script('owl-carousel-js');
//    $jsArgs[] = 'owl-carousel-js';
//
//    // Waypoints
//    wp_deregister_script('waypoints');
//    wp_register_script('waypoints', WP_MEDIA_URL . '/lib/waypoints/jquery.waypoints.js', $jsArgs, VERSION_URI,
//        false);
//    wp_enqueue_script('waypoints');
//    $jsArgs[] = 'waypoints';

    $localizeArgs = [
        'ajax' => admin_url('admin-ajax.php'),
        'url'  => get_wp_site_url(),
    ];

    wp_localize_script('main', 'site', $localizeArgs);
}

function assets() {
    wp_enqueue_style('sage/css', asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script('sage/js', asset_path('scripts/main.js'), ['jquery'], true, null);
}
add_action('wp_enqueue_scripts', 'assets', 100);


/**
 * Load conditional scripts and stylesheets for the login page.
 */
function login_load_files()
{
    wp_enqueue_style('login-css', CONTENT_URL . '/css/login.css', [], VERSION_URI);
}

/**
 * Load conditional scripts and stylesheets for the backend.
 */
function admin_load_files()
{
    wp_enqueue_style('admin-css', CONTENT_URL . '/css/admin.css', [], VERSION_URI);
    wp_enqueue_script('admin-js', CONTENT_URL . '/js/admin.js', [], VERSION_URI, false);
    wp_localize_script(
        'admin-js',
        'site',
        [
            'ajax'  => admin_url('admin-ajax.php'),
            'url'   => get_wp_site_url(),
            'icons' => ICONS,
        ]
    );
}
