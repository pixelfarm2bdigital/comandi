<?php

class Pixelfarm_WP_Body
{
    private static $instance = null;

    public static function get_instance()
    {
        if (null === self::$instance) {
            $class = __CLASS__;
            new $class;
        }

        return self::$instance;
    }

    private function __construct()
    {
        /*
        |--------------------------------------------------------------------------
        | WordPress
        |--------------------------------------------------------------------------
        */

        // Disable stuff
        add_filter('xmlrpc_enabled', '__return_false');
        remove_action('xmlrpc_rsd_apis', 'rest_output_rsd');
        remove_action('wp_head', 'rest_output_link_wp_head', 10);
        remove_action('template_redirect', 'rest_output_link_header', 11);

        // Apply hooks
        add_action('after_setup_theme', [&$this, 'after_setup_theme']);
        add_action('init', [&$this, 'init']);
        add_filter('wp_headers', [&$this, 'wp_headers']);
        add_filter('style_loader_src', [$this, 'style_script_loader_src'], 9999);
        add_filter('script_loader_src', [$this, 'style_script_loader_src'], 9999);
        add_filter('template_redirect', [&$this, 'template_redirect']);
        add_filter('the_content', [&$this, 'the_content'], 9999, 1);


        /*
        |--------------------------------------------------------------------------
        | Yoast
        |--------------------------------------------------------------------------
        */

        // Remove Yoast comments from source code
        add_action('wp_head', [$this, 'yoast_remove_source_comments'], ~PHP_INT_MAX);
    }

    public function after_setup_theme()
    {
        // Hide wp-admin bar
        // show_admin_bar(false);
    }

    public function init()
    {
        // Remove feeds, manifests, etc. from header
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_resource_hints', 2);

        // Disable WordPress emoji's
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
        add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
        add_filter('wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2);
    }

    public function disable_tinymce_emojis($plugins)
    {
        // Disable emoji's in TinyMCE
        if (is_array($plugins)) {
            return array_diff($plugins, ['wpemoji']);
        } else {
            return [];
        }
    }

    public function wp_headers($headers)
    {
        unset($headers['X-Pingback']);

        return $headers;
    }

    public function style_script_loader_src($src)
    {
        // Remove version params from any enqueued scripts or stylesheet and add version to filename
        if (stripos($src, 'ver=')) {
            $src = remove_query_arg('ver', $src);
            $src = preg_replace('/(\w+)\.(css|js)$/i', '$1.' . VERSION_URI . '.$2', $src);
        }

        return $src;
    }

    public function template_redirect()
    {
        // Remove tag, date and author archives > redirect to 404
        if (is_tag() || is_date() || is_author()) {
            global $wp_query;
            $wp_query->set_404();
        }
    }

    public function the_content($content)
    {
        // Remove empty <p> tags around block elements
        $content = force_balance_tags($content);
        $content = preg_replace(
            [
                '#<p>\s*<(div|aside|section|article|header|footer)#',
                '#</(div|aside|section|article|header|footer)>\s*</p>#',
                '#</(div|aside|section|article|header|footer)>\s*<br ?/?>#',
                '#<(div|aside|section|article|header|footer)(.*?)>\s*</p>#',
                '#<p>\s*<\/(div|aside|section|article|header|footer)#',
            ], [
            '<$1',
            '</$1>',
            '</$1>',
            '<$1$2>',
            '</$1',
        ],
            $content
        );
        $content = preg_replace('#<p>(\s|&nbsp;)*+(<br\s*/*>)*(\s|&nbsp;)*</p>#i', '', $content);

        // Remove <p> around <img>
        $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);

        // Add title attribute to links
        $content = preg_replace(
            '/(<a[^\>]+?title=[\'\"])([^\'\"]*?)([\'\"][^\>]+?>)/i',
            '$1$2 &bullet; ' . get_bloginfo('name') . '$3',
            $content
        );

        // Adjust mailto to use antispambot protection
        $content = preg_replace_callback(
            '/<a(.*?)href="mailto:(.*?)"(.*?)>(.*?)<\/a>/i',
            function ($match) {

                // When mailto has subject and/or extra email address, catch it!
                if (strpos($match[2], '?') !== false) {
                    $parts    = explode('?', $match[2]);
                    $match[2] = antispambot($parts[0]) . '?' . $parts[1];
                } else {
                    $match[2] = antispambot($match[2]);
                }

                return '<a' . $match[1] . 'href="mailto:' . $match[2] . '"' . $match[3] . '>' .
                    antispambot($match[4]) . '</a>';
            },
            $content
        );

        return $content;
    }

    public function yoast_remove_source_comments()
    {
        if (defined('WPSEO_VERSION')) {
            ob_start(function ($o) {
                return preg_replace('/^\n?\<\!\-\-.*?[Y]oast.*?\-\-\>\n?$/mi', '', $o);
            });
        }
    }
}

// Only apply all settings above for the website itself
if (!is_admin() && !in_array($GLOBALS['pagenow'], ['wp-login.php', 'wp-register.php'])) {
    Pixelfarm_WP_Body::get_instance();
}
