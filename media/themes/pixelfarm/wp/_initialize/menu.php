<?php

class Pixelfarm_WP_Menu
{
    private static $instance = null;

    public static function get_instance()
    {
        if (null === self::$instance) {
            $class = __CLASS__;
            new $class;
        }

        return self::$instance;
    }

    private function __construct()
    {
        // Apply hooks
        add_filter('init', [&$this, 'init']);
        add_filter('wp_nav_menu_args', [&$this, 'wp_nav_menu_args']);
        add_filter('nav_menu_item_id', '__return_null');
        add_filter('wp_nav_menu', [&$this, 'wp_nav_menu'], 100, 1);
    }

    function init()
    {
        // Register menu's
        register_nav_menu('main', __('Main menu', LD));
        register_nav_menu('footer', __('Footer menu', LD));
        register_nav_menu('mobile', __('Mobile menu', LD));
    }

    public function wp_nav_menu_args($args = '')
    {
        // Cleanup menu arguments
        $nav_menu_args              = [];
        $nav_menu_args['container'] = false;

        if (!$args['items_wrap']) {
            $nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
        }

        if (!$args['walker']) {
            $nav_menu_args['walker'] = new Pixelfarm_WP_Nav_Walker();
        }

        return array_merge($args, $nav_menu_args);
    }

    public function wp_nav_menu($menu)
    {
        // Remove id and classes from <ul>
        $menu = preg_replace('/<ul(.*?)>/', '<ul>', $menu);

        // Adjust title attribute
        $menu = preg_replace(
            '/(\<a.*?href\=\".*?\".*?)(\>)(.*?)(\<\/a\>)/i',
            '$1 title="$3 &bullet; ' . get_bloginfo('name') . '"$2$3$4',
            $menu
        );

        // Add itemprop="url"
        $menu = preg_replace('/<a(.*?)>(.*?)<\/a>/', '<a$1 itemprop="url">$2</a>', $menu);

        return $menu;
    }
}

class Pixelfarm_WP_Nav_Walker extends Walker_Nav_Menu
{
    private $_cpt;
    private $_archive;

    public function __construct()
    {
        add_filter('nav_menu_css_class', [&$this, 'nav_menu_css_class'], 10, 2);
        add_filter('nav_menu_item_id', '__return_null');

        $cpt            = get_post_type();
        $this->_cpt     = in_array($cpt, get_post_types(['_builtin' => false]));
        $this->_archive = get_post_type_archive_link($cpt);
    }

    private function _relative_url($input)
    {
        // Make a URL relative
        $url = parse_url($input);
        if (!isset($url['host']) || !isset($url['path'])) {
            return $input;
        }

        $site_url = parse_url(network_site_url());
        if (!isset($url['scheme'])) {
            $url['scheme'] = $site_url['scheme'];
        }

        $hosts_match   = $site_url['host'] === $url['host'];
        $schemes_match = $site_url['scheme'] === $url['scheme'];
        $ports_exist   = isset($site_url['port']) && isset($url['port']);
        $ports_match   = ($ports_exist) ? $site_url['port'] === $url['port'] : true;

        if ($hosts_match && $schemes_match && $ports_match) {
            return wp_make_link_relative($input);
        }

        return $input;
    }

    private function _url_compare($url, $rel)
    {
        // Compare URL against relative URL
        $url = trailingslashit($url);
        $rel = trailingslashit($rel);

        return ((strcasecmp($url, $rel) === 0) || $this->_relative_url($url) == $rel);
    }

    public function check_current($classes)
    {
        // Check if menu item is current page
        return preg_match('/(current[-_])|active/', $classes);
    }

    public function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {
        // Check for children
        $element->has_children = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));
        if ($element->has_children) {
            $element->classes[] = 'has-children';

            foreach ($children_elements[$element->ID] as $child) {
                if ($child->current_item_parent || $this->_url_compare($this->_archive, $child->url)) {
                    $element->classes[] = 'active';
                }
            }
        }

        // Check if child
        if (intval($element->menu_item_parent) !== 0) {
            $element->classes[] = 'is-child';
        }

        // Check if top level parent
        if (intval($depth) === 0 && isset($element->classes)) {
            $element->classes[] = 'is-top-level';
        }

        // Check if current
        $element->is_active = (!empty($element->url) && strpos($this->_archive, $element->url));
        if ($element->is_active) {
            $element->classes[] = 'active';
        }

        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    public function nav_menu_css_class($classes, $item)
    {
        // Adjust the default WordPress css classes on all menu items
        $slug = sanitize_title($item->title);

        if ($this->_cpt) {
            $classes = str_replace('current_page_parent', '', $classes);

            if ($this->_url_compare($this->_archive, $item->url)) {
                $classes[] = 'active';
            }
        }

        // Remove most core classes
        $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
        $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

        // Add `menu-<slug>` class
        $classes[] = 'menu-' . $slug;

        $classes = array_unique($classes);
        $classes = array_map('trim', $classes);

        return array_filter($classes);
    }
}

Pixelfarm_WP_Menu::get_instance();
