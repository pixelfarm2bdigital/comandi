<?php

class Pixelfarm_WP_Global
{
    private static $instance = null;

    public static function get_instance()
    {
        if (null === self::$instance) {
            $class = __CLASS__;
            new $class;
        }

        return self::$instance;
    }

    private function __construct()
    {
        // Only make an extra thumbnail image
        add_filter('intermediate_image_sizes_advanced', function ($sizes) {
            unset($sizes['medium']);
            unset($sizes['medium_large']);
            unset($sizes['large']);

            return $sizes;
        });

        // Disable WordPRess responsive srcset images
        add_filter('max_srcset_image_width', function () {
            return 1;
        });
    }
}

Pixelfarm_WP_Global::get_instance();
