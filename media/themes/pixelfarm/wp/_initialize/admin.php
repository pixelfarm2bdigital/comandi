<?php

class Pixelfarm_WP_Admin
{
    private static $instance = null;

    public static function get_instance()
    {
        if (null === self::$instance) {
            $class = __CLASS__;
            new $class;
        }

        return self::$instance;
    }

    private function __construct()
    {
        // Disable stuff
        add_filter('xmlrpc_enabled', '__return_false');
        add_filter('rest_jsonp_enabled', '__return_false');
        add_filter('rest_enabled', false);

        // Apply hooks
        add_action('init', [&$this, 'init']);
        add_action('admin_menu', [&$this, 'admin_menu'], 9999);
        add_action('pre_user_query', [&$this, 'pre_user_query']);
        add_action('admin_head-nav-menus.php', [&$this, 'admin_head_nav_menus']);
        add_filter('editable_roles', [&$this, 'editable_roles']);
        add_filter('wp_before_admin_bar_render', [&$this, 'wp_before_admin_bar_render']);
        add_filter('widgets_init', [&$this, 'widgets_init']);
        add_filter('admin_footer_text', [&$this, 'admin_footer_text']);
        add_filter('update_footer', '__return_empty_string', 11);
        add_filter('parse_query', [$this, 'exclude_pages_from_admin']);

        // Prevent post from being deleted
        add_action('wp_trash_post', [&$this, 'restrict_page_deletion'], 1);
        add_action('before_delete_post', [&$this, 'restrict_page_deletion'], 1);

        // ACF
        add_filter('acf/settings/show_admin', [$this, 'hide_acf']);
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page([
                'page_title' => get_bloginfo('name'),
                'menu_title' => get_bloginfo('name'),
                'menu_slug'  => strtolower(get_bloginfo('name')),
                'capability' => 'manage_options',
                'position'   => 25,
                'icon_url'   => 'dashicons-id-alt',
            ]);
        }

        // Yoast
        add_filter('wpseo_metabox_prio', [$this, 'yoast_lower_metabox']);

        // TinyMCE
        add_filter('tiny_mce_before_init', [&$this, 'tiny_mce_before_init']);
    }

    public function init()
    {
        // Don't delete trash automatically
        remove_action('wp_scheduled_delete', 'wp_scheduled_delete');

        load_theme_textdomain('px', get_template_directory() . '/lang');

        // Enable featured image
        add_theme_support('post-thumbnails');
        //add_image_size( 'header_image', 1920, 700,  array( 'left', 'top' ) ); Add image size called header_image with a hard crop fron the top left to 1920 x 700;


        // Remove customizer actions
        remove_action('plugins_loaded', '_wp_customize_include', 10);
        remove_action('admin_enqueue_scripts', '_wp_customize_loader_settings', 11);

        // Remove categories and tags
        unregister_taxonomy_for_object_type('category', 'post');
        unregister_taxonomy_for_object_type('post_tag', 'post');
    }

    public function admin_menu()
    {
        // Remove metaboxes dashboard
        remove_action('welcome_panel', 'wp_welcome_panel');
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
        remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
        remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
        remove_meta_box('dashboard_quick_press', 'dashboard', 'normal');
        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'normal');
        remove_meta_box('dashboard_primary', 'dashboard', 'normal');
        remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
        remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'side');

        // Remove metaboxes pages
        remove_meta_box('tagsdiv-product_tag', 'product', 'side');
        remove_meta_box('authordiv', 'post', 'normal');
        remove_meta_box('commentstatusdiv', 'post', 'normal');
        remove_meta_box('commentsdiv', 'post', 'normal');
        remove_meta_box('postcustom', 'post', 'normal');
        remove_meta_box('postexcerpt', 'post', 'normal');
        remove_meta_box('revisionsdiv', 'post', 'normal');
        remove_meta_box('tagsdiv-post_tag', 'post', 'normal');
        remove_meta_box('trackbacksdiv', 'post', 'normal');

        // Hide update message for non-administrators
        if (!current_user_can('administrator')) {
            remove_action('admin_notices', 'update_nag', 3);
            remove_filter('update_footer', 'core_update_footer');
        }

        global $submenu;

        // Adjust WordPress menu structure
        remove_menu_page('edit.php');           // Posts
        remove_menu_page('edit-comments.php');  // Comments

        if (!current_user_can('administrator')) {
            remove_menu_page('plugins.php');                        // Plugins
            remove_menu_page('tools.php');                          // Tools
            remove_menu_page('options-general.php');                // Settings
            remove_menu_page('wpseo_dashboard');                    // Yoast
            remove_menu_page('wpfront-user-role-editor-all-roles'); // WP Front Roles
            unset($submenu['themes.php'][5]);                       // Themes
        }

        // Deny access to WordPress for unauthorized administrators //!TODO - ...
        //        $currID = is_user_logged_in() ? get_current_user_id() : 0;
        //        if (current_user_can('administrator') && (int)$currID !== 1514764800) {
        //            mail(
        //                WEBDEVELOPMENT_MAIL,
        //                'Unauthorized administrator for beren.nl',
        //                'Er is een ongeauthoriseerde administrator aangemaakt die probeert WordPress te gebruiken!'
        //            );
        //            wp_die("Je bent een niet geauthoriseerde administrator. Wij zijn op de hoogte gesteld van deze zaak!");
        //            exit();
        //        }
    }

    public function pre_user_query($user_search)
    {
        global $wpdb;

        // Hide manager user from WordPress panel for non-administrator and non-manager users
        if (!current_user_can('administrator') && !current_user_can('manager')) {
            $user_search->query_where = str_replace(
                'WHERE 1=1', "WHERE 1=1 AND "
                . "{$wpdb->users}.ID IN ("
                . "SELECT {$wpdb->usermeta}.user_id "
                . "FROM $wpdb->usermeta "
                . "WHERE {$wpdb->usermeta}.meta_key = '{$wpdb->prefix}capabilities' "
                . "AND {$wpdb->usermeta}.meta_value NOT LIKE '%administrator%'"
                . "AND {$wpdb->usermeta}.meta_value NOT LIKE '%manager%'"
                . ")", $user_search->query_where
            );

        } // Hide administrator user from WordPress panel for non-administrator users
        elseif (!current_user_can('administrator')) {
            $user_search->query_where = str_replace(
                'WHERE 1=1', "WHERE 1=1 AND "
                . "{$wpdb->users}.ID IN ("
                . "SELECT {$wpdb->usermeta}.user_id "
                . "FROM $wpdb->usermeta "
                . "WHERE {$wpdb->usermeta}.meta_key = '{$wpdb->prefix}capabilities' "
                . "AND {$wpdb->usermeta}.meta_value NOT LIKE '%administrator%'"
                . ")", $user_search->query_where
            );
        }
    }

    public function admin_head_nav_menus()
    {
        // Hide metaboxes from menu page
        remove_meta_box('add-post-type-post', 'nav-menus', 'side');
    }

    public function editable_roles($roles)
    {
        // Hide administrator role
        if (isset($roles['administrator']) && !current_user_can('administrator')) {
            unset($roles['administrator']);
        }

        // Hide manager role
        if (!current_user_can('administrator') && !current_user_can('manager')) {
            unset($roles['manager']);
        }

        return $roles;
    }

    public function wp_before_admin_bar_render()
    {
        global $wp_admin_bar;

        // Remove icons from the wp-admin bar
        if ($wp_admin_bar instanceof WP_Admin_Bar) {
            $wp_admin_bar->remove_menu('wp-logo');
            $wp_admin_bar->remove_menu('updates');
            $wp_admin_bar->remove_menu('comments');
            $wp_admin_bar->remove_menu('wpseo-menu');
        }
    }

    public function widgets_init()
    {
        // Remove default widgets
        unregister_widget('WP_Widget_Custom_HTML');
        unregister_widget('WP_Widget_Media_Gallery');
        unregister_widget('WP_Widget_Pages');
        unregister_widget('WP_Widget_Links');
        unregister_widget('WP_Widget_Search');
        unregister_widget('WP_Widget_Archives');
        unregister_widget('WP_Widget_Media');
        unregister_widget('WP_Widget_Media_Audio');
        unregister_widget('WP_Widget_Media_Image');
        unregister_widget('WP_Widget_Media_Video');
        unregister_widget('WP_Widget_Meta');
        unregister_widget('WP_Widget_Calendar');
        unregister_widget('WP_Widget_Text');
        unregister_widget('WP_Widget_Categories');
        unregister_widget('WP_Widget_Recent_Posts');
        unregister_widget('WP_Widget_Recent_Comments');
        unregister_widget('WP_Widget_RSS');
        unregister_widget('WP_Widget_Tag_Cloud');
        unregister_widget('WP_Nav_Menu_Widget');

        global $wp_widget_factory;
        if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
            remove_action(
                'wp_head',
                [
                    $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
                    'recent_comments_style',
                ]
            );
        }
    }

    public function admin_footer_text()
    {
        // Add copyright to footer
        echo 'Copyright &copy; ' . date('Y') . ' Pixelfarm B.V.';
    }

    /**
     * Hide system pages for non-administrators.
     *
     * @param $wp_query
     */
    public function exclude_pages_from_admin($wp_query)
    {
        global $pagenow, $post_type;

        if (is_admin() && !current_user_can('administrator')) {
            if ('page' === $post_type) {

                $wp_query->query_vars['post__not_in'] = [
                    // Hide these pages from non administrators
                ];
            }
        }
    }

    public function restrict_page_deletion($page_id)
    {
        $undeletable_pages = [
            //  ake sure these pages can not be deleted
        ];

        if (in_array($page_id, $undeletable_pages)) {
            exit('The page you were trying to delete is protected and cannot be deleted!');
        }
    }

    public function hide_acf()
    {
        return current_user_can('administrator');
    }

    public function yoast_lower_metabox()
    {
        return 'low';
    }

    public function tiny_mce_before_init($init)
    {
        $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Blockquote=blockquote';

        return $init;
    }
}

// Only apply all settings above for the wp-admin area
if (is_admin()) {
    Pixelfarm_WP_Admin::get_instance();
}
