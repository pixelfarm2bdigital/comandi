<?php

class Pixelfarm_WP_Search
{
    private static $instance = null;

    public static function get_instance()
    {
        if (null === self::$instance) {
            $class = __CLASS__;
            new $class;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_action('init', [&$this, 'init']);
        add_filter('request', [&$this, 'request']);
        add_filter('do_parse_request', [&$this, 'do_parse_request'], -100000);

        add_theme_support('nice-search');
        if (current_theme_supports('nice-search')) {
            add_action('template_redirect', [&$this, 'template_redirect']);
        }
    }

    public function init()
    {
        global $wp_rewrite;

        // Set search base
        $wp_rewrite->search_base = 'search';
    }

    public function request($query_vars)
    {
        // Redirect empty search queries to home page
        if (isset($_GET['s']) && empty($_GET['s']) && !is_admin()) {
            $query_vars['s'] = ' ';
        }

        return $query_vars;
    }

    public function template_redirect()
    {
        global $wp_rewrite;

        if (!isset($wp_rewrite) || !is_object($wp_rewrite) || !$wp_rewrite->using_permalinks()) {
            return;
        }

        // Rewrite /?s=xxx to /search/xxx
        $search_base = $wp_rewrite->search_base;
        if (is_search() && !empty($_GET['s']) && !is_admin() && strpos($_SERVER['REQUEST_URI'],
                "/{$search_base}/") === false) {
            $url = home_url('/' . _x('search', 'search url slug', 'text-domain') . '/') . urlencode(get_query_var('s'));

            wp_redirect($url);
            exit;
        }
    }

    public function do_parse_request($return)
    {
        // WordPress does not understand the translated slug, so we un-translate it
        if (defined('ICL_LANGUAGE_CODE')) {
            $untranslate = [
                ICL_LANGUAGE_CODE . '/' . _x('search', 'search url slug', 'text-domain') => ICL_LANGUAGE_CODE . '/search',
            ];

            // WordPress uses either one of these, so patch them both
            if ($_SERVER['PATH_INFO'] ?? false) {
                $_SERVER['PATH_INFO'] = strtr($_SERVER['PATH_INFO'], $untranslate);
            }
            if ($_SERVER['REQUEST_URI'] ?? false) {
                $_SERVER['REQUEST_URI'] = strtr($_SERVER['REQUEST_URI'], $untranslate);
            }
        }

        return $return;
    }
}

Pixelfarm_WP_Search::get_instance();
