<?php

/**
 * Get all svg icon names.
 *
 * @param string $filename
 *
 * @return array
 */
function get_svg_icon_names(string $filename = 'icons'): array
{
    $contents = file_get_contents(RESOURCES_DIR . "/img/{$filename}.svg");

    preg_match_all('/id=\"icon-(.*?)\"/i', $contents, $matches);
    $icons = $matches[1] ?? [];

    sort($icons);

    return $icons;
}

/**
 * Print SVG loader.
 *
 * @param bool $black
 */
function print_svg_loader(bool $black = false): void
{
    if ($black) {
        echo file_get_contents(RESOURCES_DIR . '/img/loader-black.svg');
    } else {
        echo file_get_contents(RESOURCES_DIR . '/img/loader.svg');
    }
}

/**
 * Print SVG loader.
 *
 * @param bool $black
 *
 * @return string
 */
function get_svg_loader(bool $black = false): string
{
    if ($black) {
        return file_get_contents(RESOURCES_DIR . '/img/loader-black.svg');
    } else {
        return file_get_contents(RESOURCES_DIR . '/img/loader.svg');
    }
}
