<?php

/**
 * Session expiration.
 *
 * Set the expiration time of our (WordPress) sessions to 1 hours.
 */
function check_sessions_expiration()
{
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > (60 * 60 * 1))) {
        session_unset();
        session_destroy();
        session_start();
    }
    $_SESSION['LAST_ACTIVITY'] = time();
}

add_action('wp_session_init', 'check_sessions_expiration');
