<?php

/**
 * Get website URL.
 * This also works with WPML!
 *
 * @return string
 */
function get_wp_site_url()
{
    return esc_url(home_url());
}

/**
 * Highlight given terms in given content.
 *
 * @param string $content
 * @param array  $terms
 *
 * @return string
 */
function hightlight_terms(string $content, array $terms): string
{
    return preg_replace('/(' . implode('|', $terms) . ')/iu', '<mark>\0</mark>', $content);
}

/**
 * Show 404 page.
 */
function force_404()
{
    global $wp_query;

    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}

/**
 * Get SEO text.
 *
 * @param string $title
 *
 * @return string
 */
function get_seo_title(string $title): string
{
    $str = $title . ' &bullet; ' . get_bloginfo('name');

    if (empty(trim($title))) {
        $str = get_bloginfo('name');
    }

    return $str;
}

/**
 * Get a protected <a> link with mailto for the given email address.
 *
 * @param string $mail
 *
 * @return string
 */
function get_mailto_link(string $mail): string
{
    $antiSpambotMail = antispambot($mail);
    $attrTitle       = sprintf(__('Send an email to %s', LD), $mail);

    return '<a href="mailto:' . $antiSpambotMail . '" title="' . $attrTitle . '">' . $antiSpambotMail . '</a>';
}

/**
 * Get posts for fiven Custom Post Type.
 *
 * @param string $cpt     - Name of the Custom Post Type.
 * @param array  $options - Override defaults options.
 * @param array  $exclude - List of pages (ID's) to exclude.
 *
 * @return \WP_Query
 */
function get_custom_post_type_posts(string $cpt, array $options = [], array $exclude = []): \WP_Query
{
    $defaults = [
        'post_status'    => 'publish',
        'post_type'      => $cpt,
        'posts_per_page' => -1,
        'post__not_in'   => $exclude,
        'orderby'        => 'title',
        'order'          => 'ASC',
    ];

    $posts = new WP_Query(array_merge($defaults, $options));

    return $posts;
}



function add_plugin_path( $paths ) {
    $paths['my_plugin'] =  RESOURCES_DIR . '/field-groups/';
    return $paths;
}
add_filter( 'acfwpcli_fieldgroup_paths', 'add_plugin_path' );

