<?php

/**
 * Custom print_r() function.
 *
 * @param mixed $mixed
 * @param bool  $exit
 */
function print_rr($mixed, $exit = false)
{
    echo "<pre>";
    print_r($mixed);
    echo "</pre>";

    if ($exit) {
        exit();
    }
}

/**
 * Remove all whitespaces from string.
 *
 * @param string $str
 *
 * @return mixed
 */
function remove_all_whitespaces(string $str): string
{
    return trim(preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $str)));
}

/**
 * Recursively deletes a directory tree.
 *
 * @param string $folder         - The directory path.
 * @param bool   $keepRootFolder - Whether to keep the top-level folder.
 *
 * @return bool
 */
function delete_tree(string $folder, bool $keepRootFolder = false): bool
{
    // Handle bad arguments
    if (empty($folder) || !file_exists($folder)) {
        return true; // No such file/folder exists.
    } elseif (is_file($folder) || is_link($folder)) {
        return @unlink($folder); // Delete file/link.
    }

    // Delete all children
    $files = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($folder, \RecursiveDirectoryIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::CHILD_FIRST
    );

    foreach ($files as $fileinfo) {
        /** @var \SplFileInfo $fileinfo */
        if ($fileinfo->isDir()) {
            if (!rmdir($fileinfo->getRealPath())) {
                return false;
            }
        } else {
            if (!unlink($fileinfo->getRealPath())) {
                return false;
            }
        }
    }

    // Delete the root folder itself?
    return (!$keepRootFolder ? rmdir($folder) : true);
}

/*
 * Inserts a new key/value before the key in the array.
 *
 * @param $key
 * @param $array
 * @param $new_key
 * @param $new_value
 *
 * @return array
 */
function array_insert_before($key, array &$array, $new_key, $new_value)
{
    if (array_key_exists($key, $array)) {
        $newArray = [];
        foreach ($array as $k => $value) {
            if ($k === $key) {
                $newArray[$new_key] = $new_value;
            }
            $newArray[$k] = $value;
        }

        return $newArray;
    }

    return $array;
}

/**
 * Get days of the week.
 *
 * @return array
 */
function get_weekdays()
{
    return [
        __('Monday', LD),
        __('Tuesday', LD),
        __('Wednesday', LD),
        __('Thursday', LD),
        __('Friday', LD),
        __('Saturday', LD),
        __('Sunday', LD),
    ];
}

/**
 * Get string between
 *
 * @param string $string
 * @param string $start
 * @param string $end
 *
 * @return string
 */
function get_string_between(string $string, string $start, string $end): string
{
    $string = ' ' . $string;
    $ini    = strpos($string, $start);

    if ($ini == 0) {
        return '';
    }

    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;

    return substr($string, $ini, $len);
}

/**
 * Replace last occurent string in a string by given string.
 *
 * @param string $search
 * @param string $replace
 * @param string $subject
 *
 * @return string
 */
function str_lreplace(string $search, string $replace, string $subject): string
{
    $pos = strrpos($subject, $search);

    if ($pos !== false) {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}

/**
 * Move element in array.
 *
 * @param $arr  - Input array.
 * @param $posA - Index of element to move.
 * @param $posB - Index where to move element to.
 */
function array_move_element(&$arr, $posA, $posB)
{
    $out = array_splice($arr, $posA, 1);

    array_splice($arr, $posB, 0, $out);
}

/**
 * Flatten array.
 *
 * @param array $array
 *
 * @return array
 */
function array_flatten($array): array
{
    $result = [];

    if (!is_array($array)) {
        return $result;
    }

    array_walk_recursive($array, function ($v, $k) use (&$result) {
        $result[] = $v;
    });

    return $result;
}

/**
 * Get current URL.
 *
 * @param bool $trim_query_string
 *
 * @return string
 */
function get_current_url(bool $trim_query_string = false): string
{
    $pageURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? "https://" : "http://";
    $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];

    $url = $pageURL;

    if ($trim_query_string) {
        $urlParts = explode('?', $pageURL);
        $url      = $urlParts[0];
    }

    return $url;
}

/**
 * Output given string lowercase and connected, like: abcd-efgh-ijk
 *
 * @param string $str
 *
 * @return string
 */
function str_linkify($str): string
{
    $str = preg_replace('/\s+/', '-', strtolower(trim($str)));
    $str = preg_replace('/\'/', '', $str);

    return $str;
}

/**
 * Recursively apply array_values.
 *
 * @param array $arr
 * @param bool  $removeEmpty
 *
 * @return array
 */
function array_values_recursive(array $arr, bool $removeEmpty = true): array
{
    $arr = array_values($arr);

    for ($i = 0, $n = count($arr); $i < $n; $i++) {
        $element = $arr[$i];

        if (is_array($element)) {
            if ($removeEmpty) {
                $element = array_values(array_filter($element));
            }

            $arr[$i] = array_values_recursive($element, $removeEmpty);
        }
    }

    return $arr;
}

/**
 * Cast string to float.
 *
 * @param string $str
 *
 * @return float
 */
function str_to_float(string $str): float
{
    return floatval(str_replace(',', '.', $str));
}

/**
 * Converts a letter to a digit.
 *
 * For example:
 * - A or a becomes 1
 * - Z or z become 26.
 *
 * @param string $str
 *
 * @return int
 */
function str_to_number(string $str): int
{
    $number = 0;

    if ($str) {
        $number = ord(substr(strtolower($str), 0, 1)) - 96;
    }

    return $number;
}

/**
 * Decrypt given string.
 *
 * @param string $str - String to decrypt.
 *
 * @return string|false
 */
function strDecrypt($str)
{
    $encrypt_method = "AES-256-CBC";
    $secret_key     = 'sdjkfh2r428376423798yksdjf';
    $secret_iv      = 'sad1fb2378geriuewgo8ewgflk';

    $key = hash('sha256', $secret_key);
    $iv  = substr(hash('sha256', $secret_iv), 0, 16);

    return openssl_decrypt(base64_decode($str), $encrypt_method, $key, 0, $iv);
}

/**
 * Encrypt given string.
 *
 * @param string $str - String to encrypt.
 *
 * @return string|false
 */
function strEncrypt($str)
{
    $encrypt_method = "AES-256-CBC";
    $secret_key     = 'sdjkfh2r428376423798yksdjf';
    $secret_iv      = 'sad1fb2378geriuewgo8ewgflk';

    $key    = hash('sha256', $secret_key);
    $iv     = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_encrypt($str, $encrypt_method, $key, 0, $iv);

    if (!$output) {
        return false;
    }

    return base64_encode($output);
}

/**
 * Get phone number link for the [href] in an <a> element.
 *
 * @param $phoneNumber
 *
 * @return string
 */
function get_phone_link($phoneNumber): string
{
    return '+' . ltrim(preg_replace('/[^0-9]/', '', $phoneNumber), '0');
}
