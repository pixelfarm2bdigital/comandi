<?php

/**
 * Disable automatic updates.
 */
if (get_option('gform_enable_background_updates')) {
    update_option('gform_enable_background_updates', false, true);
}
add_filter('gform_disable_auto_update', '__return_true');

/**
 * Change menu name.
 */
add_action('admin_menu', function () {
    global $menu;

    $menu['16.9'][0] = 'Gravity Forms';
}, 9999);

/**
 * Change menu order.
 *
 * @param $menu_order
 *
 * @return array
 */
add_filter('custom_menu_order', '__return_true');
add_filter('menu_order', function ($menu_order) {
    $oldPos = array_search('gf_edit_forms', $menu_order);
    $newPos = array_search('upload.php', $menu_order) + 1;

    array_move_element($menu_order, $oldPos, $newPos);

    return $menu_order;
}, 9999);


add_filter( 'gform_phone_formats', 'nl_phone_format' );
function nl_phone_format( $phone_formats ) {
    $phone_formats['nl'] = array(
        'label'       => 'Nederlands',
        'mask'        => false,
        'regex'       => '/^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9])((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]$/',
        'instruction' => false,
    );

    return $phone_formats;
}

add_filter( 'gform_confirmation_anchor', '__return_true' );
