<?php

/**
 * Exit application if error occurs in development mode. Else
 * send me a mail.
 *
 * @param string $method
 * @param string $line
 * @param string $string
 *
 * @return bool
 */
function handle_error($method, $line, $string)
{
    $message = ""
        . "Function: $method" . "<br>"
        . "Line: $line" . "<br>"
        . "<br>"
        . "$string";

    // Exit and show error
    if (DEVELOPMENT_MODE) {
        exit($message);
    }

    // Send error mail, because we are in production
    return send_error_mail($message);
}

/**
 * Send error mail.
 *
 * @param string $message
 *
 * @return bool
 */
function send_error_mail($message)
{
    return mail(EMAIL_DEVELOPMENT_TEAM, 'Error in: ' . WP_SITEURL, $message);
}
