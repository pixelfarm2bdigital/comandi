<?php

// Register ajax functions
require_once 'default.php';
require_once 'error.php';
require_once 'sessions.php';
require_once 'svg.php';
require_once 'wp.php';
require_once 'gravity-forms.php';
