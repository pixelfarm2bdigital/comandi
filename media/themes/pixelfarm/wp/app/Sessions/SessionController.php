<?php

namespace App\Sessions;

/**
 * Wrapper class for WP_Session that holds functionality to get, set and delete current sessions and sessiondata
 * and that adds the use of serialize() and unserialize().
 */
class SessionController
{
    /**
     * Get all sessions.
     *
     * @return array
     */
    public function get_sessions()
    {
        return $_SESSION;
    }

    /**
     * Get session by given key.
     *
     * @param string $key - Unique identifier of the session.
     *
     * @return bool|mixed
     */
    public function get($key)
    {
        return isset($_SESSION[$key]) ? unserialize($_SESSION[$key]) : false;
    }

    /**
     * Store data in a session with given key and value.
     *
     * @param string $key   - Unique identifier for the session.
     * @param mixed  $value - The value for the session.
     *
     * @return void
     */
    public function set($key, $value)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }

        $_SESSION[$key] = serialize($value);
    }

    /**
     * Delete session by given key.
     *
     * @param string $key - Unique identifier of the session.
     *
     * @return void
     */
    public function delete($key)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

    /**
     * Remove all existing sessions.
     *
     * @param bool $deleteSpecials - Indicates whether or not to delete specials sessions as well.
     *
     * @return void
     */
    public function delete_all($deleteSpecials = false)
    {
        $specialKeys = [
            'LAST_ACTIVITY',
        ];

        if (count($_SESSION) > 0) {
            foreach ($_SESSION as $k => $v) {
                if ($deleteSpecials !== true) {
                    if (in_array($k, $specialKeys, true)) {
                        continue;
                    }
                }

                unset($_SESSION[$k]);
            }
        }

        if ($deleteSpecials === true) {
            $_SESSION = [];
        }
    }
}
