<?php

register_sidebar([
    'id'            => 'default-sidebar',
    'name'          => 'Default sidebar',
    'class'         => '',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '',
    'after_title'   => '',
]);
