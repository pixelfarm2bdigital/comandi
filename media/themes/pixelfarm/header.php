<!doctype html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php wp_title() ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">

        <!--//!TODO - Add favicon through https://realfavicongenerator.net/ -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo WP_SITEURL ?>/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo WP_SITEURL ?>/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo WP_SITEURL ?>/favicon-16x16.png">
        <link rel="manifest" href="<?php echo WP_SITEURL ?>/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#ffcf42">
        <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name') ?>">
        <meta name="application-name" content="<?php bloginfo('name') ?>">
        <meta name="theme-color" content="#ffcf42">

        <?php wp_head() ?>
        <script>
            //!TODO - Place Google Analytics code -->
        </script>
    </head>
    <body itemscope itemtype="https://schema.org/WebPage" <?php body_class(); ?>>
        <div id="wrapper">
            <div id="header">
                <div id="menu" class="nav food">
                    <section class="section">
                        <nav itemscope itemtype="https://schema.org/SiteNavigationElement" role="navigation">
                            <?php if (has_nav_menu('main')) : ?>
                                <?php wp_nav_menu(['theme_location' => 'main', 'container' => false]) ?>
                            <?php endif; ?>
                        </nav>
                    </section>
                </div>
            </div>
