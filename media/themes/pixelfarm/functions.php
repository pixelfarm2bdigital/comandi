<?php

// Require vendor
require_once ABSPATH . 'vendor/autoload.php';

// Require functions
require_once 'wp/functions/assets.php';
require_once 'wp/functions/functions.php';

// Initialize WordPress with our custom settings
require_once 'wp/initialize.php';

// Enqueue scripts and stylesheets
require_once 'wp/enqueue.php';

// Initialize all hooks, ajax functions, shortcodes, widgets, sidebars, etc..
require_once 'wp/custom-post-types/custom-post-types.php';
require_once 'wp/shortcodes/shortcodes.php';
require_once 'wp/sidebars/sidebars.php';
require_once 'wp/widgets/widgets.php';
require_once 'wp/ajax/ajax.php';

if( function_exists('acf_add_options_page') ) {

    $option_page = acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title' 	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability' 	=> 'edit_posts',
        'redirect' 	=> false
    ));

}
