<?php
get_header();
the_post();
?>
    <article class="404" itemscope itemtype="https://schema.org/CreativeWork">
        <header>
            <h1 itemprop="headline"><?php _e('Page not found', LD) ?></h1>
        </header>
        <div class="content-wrapper">
            <section>
                <div>
                    <h3><?php _e('The page your are looking for cannot be found') ?></h3>
                    <p>Blablabla...</p>
                </div>
            </section>
        </div>
    </article>
<?php
get_footer();
