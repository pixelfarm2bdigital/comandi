<?php
get_header();
the_post();
?>

    <article class="frontpage" itemscope itemtype="https://schema.org/CreativeWork">
        <header>
            <h1 itemprop="headline"><?php echo get_the_title() ?></h1>
        </header>
        <div class="content-wrapper section" itemprop="text">
            <section>
                <div class="editor">
                    <?php the_content() ?>
                </div>
            </section>
            <aside id="sidebar" itemscope itemtype="https://schema.org/WPSideBar" role="complementary"></aside>
        </div>
    </article>
<?php
get_footer();
