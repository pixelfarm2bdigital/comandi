
            <div id="footer">
                <section class="section">
                    <nav itemscope itemtype="https://schema.org/SiteNavigationElement" role="navigation">
                        <?php if (has_nav_menu('footer')) : ?>
                            <?php wp_nav_menu(['theme_location' => 'footer', 'container' => false]) ?>
                        <?php endif; ?>
                    </nav>
                    <a id="scrollToTop"><?php _e('Back to top', LD) ?></a>
                </section>
            </div>
        </div>
        <div id="mobile-menu">
            <div class="top">
                <div class="wrap">
                    <div class="previous">
                        <div>
                            <svg class="icon">
                                <use xlink:href="<?php //echo ICONS . '#icon-...' ?>"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="close">
                        <div>
                            <svg class="icon">
                                <use xlink:href="<?php //echo ICONS . '#icon-...' ?>"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="logo">
                        <a href="<?php echo esc_url(home_url('/')) ?>">
                            <img src="<?php //echo CONTENT_URL . '....svg' ?>" alt="<?php bloginfo('description') ?>">
                        </a>
                    </div>
                </div>
            </div>
            <nav itemscope itemtype="https://schema.org/SiteNavigationElement" role="navigation">
                <?php if (has_nav_menu('mobile')) : ?>
                    <?php wp_nav_menu(['theme_location' => 'mobile', 'container' => false]) ?>
                <?php endif; ?>
            </nav>
        </div>
        <?php wp_footer() ?>
    </body>
</html>
