<?php

/*
 * This file is only called after a fresh WordPress install.
 */

class Pixelfarm_WP_Install
{
    private static $instance = null;

    /**
     * @return self
     */
    public static function get_instance()
    {
        if (null === self::$instance) {
            $class = __CLASS__;
            new $class;
        }

        return self::$instance;
    }

    /**
     * Pixelfarm_WP_Install constructor.
     */
    private function __construct()
    {
        add_action('wp_install', [$this, 'wp_install'], ~PHP_INT_MAX, 1);

        $this->clean_up();
    }

    /**
     * After WordPress installation...
     */
    public function wp_install()
    {
        $this->remove_default_post();
        $this->remove_default_comment();
        $this->adjust_default_page();

        $this->set_theme();
        $this->set_permalinks();

        $this->activate_plugins();
        $this->apply_plugin_licenses();
        $this->set_plugin_settings();

        $this->apply_settings();

        $this->flush();
    }

    /**
     * Remove default post.
     */
    protected function remove_default_post()
    {
        wp_delete_post(1, true);
    }

    /**
     * Remove default comment.
     */
    protected function remove_default_comment()
    {
        wp_delete_comment(1, true);
    }

    /**
     * Adjust page title and slug of default created page.
     */
    protected function adjust_default_page()
    {
        wp_update_post([
            'ID'           => 2,
            'post_title'   => 'Home',
            'post_name'    => 'home',
            'post_content' => '',
        ]);
    }

    /**
     * Set our own default theme.
     */
    protected function set_theme()
    {
        add_filter('stylesheet', 'pixelfarm');
        add_filter('template', 'pixelfarm');
    }

    /**
     * Set permalinks to use post name.
     */
    protected function set_permalinks()
    {
        global $wp_rewrite;

        $wp_rewrite->set_permalink_structure('/%postname%/');
        $wp_rewrite->flush_rules();
    }

    /**
     * Activate all plugins.
     */
    protected function activate_plugins()
    {
        if (!function_exists('get_plugins')) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        activate_plugins(array_keys(get_plugins()));
    }

    /**
     * Apply (PRO) licenses of the installed plugins.
     */
    protected function apply_plugin_licenses()
    {
        // Advanced Custom Fields PRO
        acf_pro_update_license('b3JkZXJfaWQ9NDkxNDJ8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE1LTAyLTAzIDEzOjQ3OjIw');

        // Gravity Forms
        GFFormsModel::save_key('1840db68c0d1ebd3022fe18a3e8bfa5b');
    }

    /**
     * Apply plugin specific settings.
     */
    protected function set_plugin_settings()
    {
        // WP Original Media Path
        update_option('wpomp_mode', 1);
        update_option('upload_path', 'media/uploads', true);
        update_option('upload_url_path', home_url('/media/uploads'), true);
//        rename(ABSPATH . 'media/uploads', RESOURCES_DIR . '/uploads');

        // Gravity Forms
        update_option('rg_gforms_disable_css', true, true);
        update_option('rg_gforms_enable_html5', true, true);
        update_option('gform_enable_noconflict', true, true);
        update_option('rg_gforms_currency', 'EUR', true);
        update_option('gform_enable_background_updates', false, true);
        update_option('gform_enable_toolbar_menu', false, true);
        update_option('rg_gforms_enable_akismet', false, true);
        update_option('gform_enable_logging', false, true);
    }

    /**
     * Adjust all WordPress settings to our own settings.
     */
    protected function apply_settings()
    {
        // Algemeen
        update_option('blogdescription', '');
        update_option('admin_email', EMAIL_DEVELOPMENT_TEAM);
        update_option('new_admin_email', EMAIL_DEVELOPMENT_TEAM);
        update_option('users_can_register', 0);
        // default role
        //update_option('WPLANG', 'nl_NL');
        update_option('timezone_string', 'Europe/Amsterdam');
        update_option('gmt_offset', '');
        update_option('date_format', 'j F Y');
        update_option('time_format', 'H:i');
        update_option('links_updated_date_format', 'j F Y H:i');
        update_option('start_of_week', 1);

        // Schrijven
        update_option('default_category', 1);
        update_option('default_email_category', 1);
        update_option('default_link_category', 0);
        update_option('default_post_format', 0);
        update_option('mailserver_url', '');
        update_option('mailserver_port', 0);
        update_option('mailserver_login', '');
        update_option('mailserver_pass', '');
        update_option('ping_sites', '');

        // Lezen
        update_option('show_on_front', 'page');
        update_option('page_on_front', 2);

        // Reacties
        update_option('default_pingback_flag', '');
        update_option('default_ping_status', 'closed');
        update_option('default_comment_status', 'closed');
        update_option('require_name_email', 1);
        update_option('comment_registration', 1);
        update_option('close_comments_for_old_posts', '');
        update_option('close_comments_days_old', 14);
        update_option('thread_comments', '');
        update_option('thread_comments_depth', 5);
        update_option('page_comments', '');
        update_option('comments_per_page', 50);
        update_option('default_comments_page', 'newest');
        update_option('comment_order', 'desc');
        update_option('comments_notify', 1);
        update_option('moderation_notify', 1);
        update_option('comment_moderation', 1);
        update_option('comment_whitelist', 1);
        update_option('comment_max_links', 0);
        update_option('show_avatars', 1);
        update_option('avatar_rating', 'G');
        update_option('avatar_default', 'mystery');

        // Media
        update_option('uploads_use_yearmonth_folders', 1);

        // Permalinks
        update_option('category_base', '');
        update_option('tag_base', '');
    }

    /**
     * Removes rewrite rules and then recreate rewrite rules.
     */
    protected function flush()
    {
        flush_rewrite_rules();
        wp_cache_flush();
    }

    /**
     * Clean up crew.
     */
    public function clean_up()
    {
        @unlink(ABSPATH . 'readme.html');
        @unlink(ABSPATH . 'license.txt');
    }
}

Pixelfarm_WP_Install::get_instance();
