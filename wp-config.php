<?php

/* Site state */

require_once 'wp-config.local.php';

define('VERSION', '0.0.1');

if (!defined('DEVELOPMENT_MODE')) define('DEVELOPMENT_MODE', false);

if(!DEVELOPMENT_MODE) {
    define('DOMAIN', $_SERVER['HTTP_HOST']);
    define('DOMAIN_URL', 'https://' . DOMAIN);
}

define('CONTENT_URL', DOMAIN_URL . '/resources');
define('RESOURCES_DIR', ABSPATH . 'resources');

/* Email addresses */
define('EMAIL_DEVELOPMENT_TEAM', 'development@pixelfarm.nl');


/* - - - - - - - - - - - - - - - - FROM HEREON DON'T EDIT ANYTHING - - - - - - - - - - - - - - - - */


/* Force SSL client-side when server-side fails */
if(!DEVELOPMENT_MODE) { // remove need for SSL on development enviroments
    if (!(
        isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1)
        ||
        (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
    )) {
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: https://' . DOMAIN . '/');
        exit();
    }
}


/* Default Php settings */
define('PIXELFARM_TIMEZONE', 'Europe/Amsterdam');
date_default_timezone_set(PIXELFARM_TIMEZONE);
ini_set('default_charset', 'UTF-8');
if (!defined('ABSPATH')) {
    define('ABSPATH', __DIR__ . '/');
}


/* Security */
ini_set('expose_php', 0);
header_remove('X-Powered-By');
ini_set('session.cookie_httponly', 1);


/* Error reporting */
ini_set('log_errors', 'Off');
define('WP_DEBUG_LOG', false);
if (DEVELOPMENT_MODE) {
    ini_set('display_errors', 'On');
    ini_set('error_reporting', E_ALL);
    define('WP_DEBUG', true);
    define('WP_DEBUG_DISPLAY', true);
    define('VERSION_URI', time());
} else {
    ini_set('display_errors', 'Off');
    ini_set('error_reporting', 0);
    define('WP_DEBUG', false);
    define('WP_DEBUG_DISPLAY', false);
    define('VERSION_URI', substr(md5(VERSION), 0, 6));
}


/* WordPress settings */
define('COOKIE_DOMAIN', DOMAIN);
define('WP_SITEURL', DOMAIN_URL);
define('WP_HOME', DOMAIN_URL);
define('LD', 'pixelfarm');

define('WP_DEFAULT_THEME', 'pixelfarm');
define('AUTOMATIC_UPDATER_DISABLED', true);
define('WP_AUTO_UPDATE_CORE', false);
define('DISALLOW_FILE_EDIT', true);
define('EMPTY_TRASH_DAYS', 9999999);
define('WP_MEMORY_LIMIT', '64M');
define('WP_POST_REVISIONS', 5);

define('WP_CONTENT_FOLDERNAME', 'media');
define('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
define('WP_CONTENT_URL', WP_SITEURL . '/' . WP_CONTENT_FOLDERNAME);
define ('WP_MEDIA_URL', WP_SITEURL . '/media');
define('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins');
define('WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins');
define('ICONS', CONTENT_URL . '/img/icons.' . VERSION_URI . '.svg');

define('AUTH_KEY', 'K:H@[ Wo-.9glyKq/V{UEpPi#oEt3hOxwqR[82ky.FfI&fcfmFJ>&oNkpl5Q/j[u');
define('SECURE_AUTH_KEY', '1_<%Ck:f38)@JOgd=1q?##nm4ZixXM#G/0R5wHJC8mWfUujzin7F6[Ue#u6GG]fm');
define('LOGGED_IN_KEY', 'PH2?w }]4WCh4-1ylI`ENTJuXkp:0I&vi-<cdTY-U#bbYEy0xS&T[SOsZj_toX_f');
define('NONCE_KEY', 'p.,%90za=vD.}F6m.b^*xV>!s%9t)b&~vlJNufnASQ|Iu)+9;N>mn/^^HU(/g?oU');
define('AUTH_SALT', 'Mvx,E)q<KU(e=j91I3M:tH VOzi,+Y3e.:@rhN~As;V2=~PWgJ:=dkP>L_L?T*i]');
define('SECURE_AUTH_SALT', '[2QGf)X9!E6zVH~/3|>)kJ3t97xq]fK/4X1jC[|-|Uh;(_c7i>a;bKm{D]Of3f@w');
define('LOGGED_IN_SALT', 'AV-Vp0]wK %S=%D=RS@EwE0m,KECHRV*qv{|zk`iI Wl)Hu@b[UWp|s:/M&hFQq<');
define('NONCE_SALT', 's!z<[@?zNfqSr2J_*8An9G0VImXA+X)`FhN[o|w1d[yqGv[1*5Y*sM2H{c]Ap!ny');


/* Require private settings */


/* Initialize WordPress */
require_once(ABSPATH . 'wp-settings.php');
